#####################
## GitLab Projects ##
#####################
## Prerequisites: GitLab Access Token
## git, curl and jq package installed
 
## -------------------------------------
## Create and update a project in GitLab
## -------------------------------------
 
## Create a project in GitLab
curl \
  --silent \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request POST \
  "https://gitlab.com/api/v4/projects?name=${GITLAB_PROJECT_NAME}&visibility=public&initialize_with_readme=true"

## Getting project ID just after creation (!)
PROJECT_ID=$(curl --silent --header "Authorization: Bearer ${GITLAB_TOKEN}" \
-XPOST "https://gitlab.com/api/v4/projects?name=${GITLAB_PROJECT_NAME}&visibility=public&initialize_with_readme=true" | jq '.id') 


# get project by name
## <YOUR-NAMESPACE>%2F<YOUR-PROJECT-NAME> --> %2F is / url-encoded
PR_ID=$(
  curl \
   --silent \
   --header "Authorization: Bearer ${GITLAB_TOKEN}" \
   --request GET \
   "https://gitlab.com/api/v4/projects/ealebed%2F${GITLAB_PR_NAME}" | jq '.id')

echo ${PR_ID}

## Get the project details (need to know project ID).
curl \
  --silent \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request GET \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}" | jq '.'
 
## Edit project and add a description (need to know project ID).
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request PUT \
  --data '{"description":"Gitlab Project" }' \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}" | jq '.description'

## Create one file in your GitLab project repository
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request POST \
  --data '{"branch": "main", "author_email": "ealebed@gmail.com", "author_name": "Yevhen Lebid", "content": "file created through api", "commit_message": "myfile1.txt added"}' \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/myfile1%2Etxt" | jq '.'

## Update one file in your GitLab project repository
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request PUT \
  --data '{"branch": "main", "author_email": "ealebed@gmail.com", "author_name": "Yevhen Lebid", "content": "file updated through api", "commit_message": "myfile1.txt updated"}' \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/myfile1%2Etxt" | jq '.'

## Create one file inside a folder in your GitLab project repository
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request POST \
  --data '{"branch": "main", "author_email": "ealebed@gmail.com", "author_name": "Yevhen Lebid", "content": "file created through api", "commit_message": "myfile2.txt added inside mydir1"}' \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/mydir1%2Fmyfile2%2Etxt" | jq '.'

## List all files and directory in your GitLab project repository
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request GET \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/tree" | jq '.'

## Delete one file from your GitLab project repository
curl \
  --silent \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request DELETE \
  --data '{"branch": "main", "author_email": "ealebed@gmail.com", "author_name": "Yevhen Lebid", "commit_message": "myfile1.txt deleted"}' \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/myfile1%2Etxt"

## Create one variable in project
curl \
  --silent \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request POST \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/variables" \
  --form "key=NEW_VARIABLE" --form "value=new value"

## Create variable with JSON value (from file) in project
curl \
  --silent \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request POST \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/variables" \
  --form "variable_type=file" --form "key=CREDENTIALS_VARIABLE" --form "value=$(cat credentials.json)"

## Get all variables from project and store in file
VARIABLES_JSON=$(curl \
  --silent \
  --header "Authorization: Bearer ${GITLAB_TOKEN}" \
  --request GET \
  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/variables")

echo "${VARIABLES_JSON}" | jq -r "map(\"\(.key)=\(.value|tostring)\") | .[]" > .env

#============================ add TF WS to TF PS
curl \
  --header "Authorization: Bearer ${TF_TOKEN}" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data '{ "data": [{"id": "ws-rPMt5qx4EChuLpTN", "type": "workspaces"}]}' \
  "https://app.terraform.io/api/v2/policy-sets/${POLICY_SET}/relationships/workspaces"

