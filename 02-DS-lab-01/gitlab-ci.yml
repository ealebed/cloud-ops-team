stages:
  - provision_notebook
  - initialize
  - build-images
  - pack-pipeline
  - run-pipeline

variables:
  TFC_ADDR: app.terraform.io # Terraform Enterprize (Cloud) host
  TFC_ORG: ealebed # Terraform Enterprise (Cloud) organization name
  TFC_WORKSPACE: gitlab-tfc-demo # Name of Terraform Enterprise (Cloud) workspace with CLI-driven workflow
  TF_CODE_DIR: terraform-code # Directory with terraform code for Vertex AI notebook
  LOCATION: us-central1 # Regional location of the repository where the image is stored AND where the pipeline should be runned
  HOSTNAME: "${LOCATION}-docker.pkg.dev" # Artifact registry hostname
  REPOSITORY_NAME: "vertex-docker-repo" # Name of the repository where the image is stored
  T_IMAGE: "${HOSTNAME}/${VERTEX_PROJECT_ID}/${REPOSITORY_NAME}/training" # Training docker image's name
  S_IMAGE: "${HOSTNAME}/${VERTEX_PROJECT_ID}/${REPOSITORY_NAME}/serving" # Serving docker image's name 
  BUCKET_NAME: coe-ie-dsg-training-lab-vertex-pipelines-root-bucket # Bucket for storing JSON-pipelines. Also can be used as pipeline_root_bucket

default:
  image: gcr.io/google.com/cloudsdktool/cloud-sdk:373.0.0
  tags:
    - mlops

# Generate 'credentials.tfrc.json' files from template using variables
.terraform-login-script: &terraform-login-script
  - mkdir -p ${HOME}/.terraform.d
  - >-
    sed -e "s/my-tf-addr/${TFE_ADDR}/" -e "s/my-token/${TFE_TOKEN}/" 
    < ${CI_PROJECT_DIR}/config_templates/credentials.tfrc.json.template > ${HOME}/.terraform.d/credentials.tfrc.json

# Prepare google credentials for building images with kaniko
.kaniko-before-script: &kaniko-before-script
  - export GOOGLE_APPLICATION_CREDENTIALS=/kaniko/kaniko-secret.json
  - base64 -d ${GCP_SA_KEY} > ${GOOGLE_APPLICATION_CREDENTIALS}

# Get service account key from secter ENV variables and authorize in GCP
.gcloud-before-script: &gcloud-before-script
  - base64 -d ${GCP_SA_KEY} > google_application_credentials_file.json
  - gcloud auth activate-service-account --key-file google_application_credentials_file.json
  - export GOOGLE_APPLICATION_CREDENTIALS=google_application_credentials_file.json
  - gcloud config set project ${VERTEX_PROJECT_ID}

# 'terraform-*' steps should run only if *.tf files changes
init-notebooks:
  stage: provision_notebook
  script:
  - *terraform-login-script
  - terraform -chdir=tf_code init -input=false
  - terraform -chdir=tf_code plan
  - terraform -chdir=tf_code apply -auto-approve
  only:
    changes:
    - tf_code/**/*
  artifacts:
    paths:
    - "tf_code/.terraform/*"
    - "tf_code/.terraform.lock.hcl"
    when: always
  when: manual

cleanup-notebook:
  stage: provision_notebook
  script:
  - *terraform-login-script
  - terraform -chdir=tf_code destroy -auto-approve
  only:
    changes:
      - tf_code/**/*
  needs: [ "init-notebooks" ]
  dependencies:
    - init-notebooks
  when: manual

# next steps should run always
# generate readable/understandable VERSION from date/time instead using git SHA
init:
  stage: initialize
  script:
    - echo "VERSION=$(date -u '+%y%m%d-%H%M')" >> build.env
  artifacts:
    reports:
      dotenv: build.env

# build training docker image and push to google artifact registry
# build-training-image:
#   stage: build-images
#   image:
#     name: gcr.io/kaniko-project/executor:v1.8.0-debug
#     entrypoint: [""]
#   script:
#     - *kaniko-before-script
#     - >-
#       /kaniko/executor
#       --context "${CI_PROJECT_DIR}"
#       --dockerfile "${CI_PROJECT_DIR}/Dockerfile.serving"
#       - --cache=true
#       --destination "${T_IMAGE}:${VERSION}"
#   needs: [ "init" ]
#   dependencies:
#     - init

# build serving docker image and push to google artifact registry
# build-serving-image:
#   stage: build-images
#   image:
#     name: gcr.io/kaniko-project/executor:v1.8.0-debug
#     entrypoint: [""]
#   script:
#     - *kaniko-before-script
#     - >-
#       /kaniko/executor
#       --context "${CI_PROJECT_DIR}"
#       --dockerfile "${CI_PROJECT_DIR}/Dockerfile.serving"
#       - --cache=true
#       --destination "${S_IMAGE}:${VERSION}"
#   needs: [ "init" ]
#   dependencies:
#     - init

# compile versioned JSON pipeline from 'python.py' with substitutions and upload it into google bucket
# (!) This step should be checked with MLOps in customer ENV
# pack-pipeline:
#   stage: pack-pipeline
#   script:
#     - *gcloud-before-script
#     - apt-get install gettext-base
#     - pip3 install kfp --upgrade --quiet && export PATH=${PATH}:~/.local/bin
#     - pip3 install --quiet --no-cache-dir -r requirements.txt
#     - >-
#       envsubst '${VERSION} ${T_IMAGE} ${S_IMAGE} ${BUCKET_NAME} ${VERTEX_PROJECT_ID} ${LOCATION}'
#       < ${CI_PROJECT_DIR}/pipeline.py > ${CI_PROJECT_DIR}/pipeline-${VERSION}.py
#     - echo "PIPELINE_NAME=mlmd-pipeline-${VERSION}" >> build.env
#     # Compile pipeline and store it in bucket
#     - python3 pipeline-${VERSION}.py
#     - gsutil cp *pipeline*.json gs://${BUCKET_NAME}/pipelines-json/
#   needs: [ "init", "build-training-image", "build-serving-image" ]
#   dependencies:
#     - init
#   artifacts:
#     reports:
#       dotenv: build.env
#     paths:
#       - "*pipeline*.json"

# run pipeline (just compiled or previosly uploaded to bucket)
# (!) This step should be checked with MLOps in customer ENV
# run-lab-pipeline:
#   stage: run-pipeline
#   script:
#     - *gcloud-before-script
#     - echo "Upload just created pipeline"
#     - export PIPELINE_TO_RUN=${PIPELINE_NAME}
#     - echo "Run pipeline ${PIPELINE_TO_RUN}"
#     - apt-get install gettext-base
#     - pip3 install google-cloud-aiplatform --quiet
#     - >-
#       envsubst '${VERTEX_PROJECT_ID} ${LOCATION} ${PIPELINE_TO_RUN}' 
#       < ${CI_PROJECT_DIR}/run-pipeline.tmpl > ${CI_PROJECT_DIR}/run-pipeline.py
#     - python3 run-pipeline.py
#   needs: [ "init", "pack-pipeline" ]
#   dependencies:
#     - init
#     - pack-pipeline
