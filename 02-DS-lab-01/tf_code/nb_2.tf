# resource "google_service_account" "custom_service_account_2" {
#   project      = local.local_data.gcp_project
#   account_id   = "custom-sa-2"
#   display_name = "Custom SA nb2"
#   description  = "Custom Service Account for testing purpose"
# }

# resource "google_project_iam_member" "project_2" {
#   for_each = toset([ 
#     "roles/storage.objectViewer",
#     "roles/compute.admin"
#   ])
  
#   project = local.local_data.gcp_project
#   role    = each.key
#   member  = "serviceAccount:${google_service_account.custom_service_account_2.email}"
# }

# resource "google_notebooks_instance" "nb_instance_2" {
#   project         = local.local_data.gcp_project
#   name            = "lab-1-test-notebook-2"
#   location        = local.local_data.gcp_zone
#   machine_type    = "n1-standard-1"
#   # disk_encryption = "CMEK" // Error: Error waiting to create Instance: Error waiting for Creating Instance: Error code 3, message: kmsKey field is required for CMEK disk encryption.
#   network         = local.local_data.network_id
#   subnet          = local.local_data.subnet_id
#   service_account = "${google_service_account.custom_service_account_2.email}"
#   labels          = {
#     "instance": "lab_1_notebook_2"
#   }
#   no_public_ip    = false
#   no_proxy_access = false

#   container_image {
#     repository = "gcr.io/deeplearning-platform-release/base-cpu"
#     tag        = "latest"
#   }
# }
