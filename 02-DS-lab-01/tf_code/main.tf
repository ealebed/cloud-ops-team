terraform {
  required_version = ">= 0.13"
  required_providers {

    google = {
      source  = "hashicorp/google"
      version = ">= 3.53, < 5.0"
    }
  }
}

# variables
variable "varSet" {}

locals {
  local_data = jsondecode(var.varSet)
}

provider "google" {
  project = local.local_data.gcp_project
  region  = local.local_data.gcp_region
  zone    = local.local_data.gcp_zone
}
