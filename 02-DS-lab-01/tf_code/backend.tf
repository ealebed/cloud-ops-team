terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "ealebed"

    workspaces {
      name = "3rd-layer-workspace-1"
    }
  }
}
