// create lab resources
module "test-vpc-module-1" {
  source       = "terraform-google-modules/network/google"
  project_id   = local.local_data.gcp_project
  network_name = "demo-network-1"
  mtu          = 1460
  
  subnets      = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.10.10.0/24"
      subnet_region = local.local_data.gcp_region
    }
  ]
}

// create workspace for notebook(s)
module "tfe-module-1" {
  source                  = "./modules/tfe"

  tfe_organization        = local.local_data.tfe_organization
  tfe_workspace           = "3rd-layer-workspace-1"

  credentials_file        = local.local_data.credentials_file

  tfe_variables_set       = jsonencode({
    "gcp_project" = local.local_data.gcp_project,
    "gcp_region"  = local.local_data.gcp_region,
    "gcp_zone"    = local.local_data.gcp_zone,
    "network_id"  = module.test-vpc-module-1.network_id,
    "subnet_id"   = module.test-vpc-module-1.subnets_ids[0]}
  )
}

# TODO: all this staff via cURL
# Just for creating variable in GitLab repo
# data "local_file" "credentials_1" {
#   filename = local.local_data.credentials_file
# }

# Add a project(s) owned by the user. A project can either be created in a group or user namespace.
# More info: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project#schema

# resource "gitlab_project" "lab_project_1" {
#   name = "02-DS-lab-01"
#   description = "Data science codebase for lab-1 env"
#   visibility_level = "public"
# }

# Add a variable(s) to the project(s)
# resource "gitlab_project_variable" "lab_project_variable_1" {
#   project       = gitlab_project.lab_project_1.id
#   key           = "LAB_G_KEY"
#   value         = data.local_file.credentials_1.content
#   variable_type = "file" # (String) The type of a variable. Available types are: `env_var` (default) and `file`
  
#   # protected (Boolean) If set to `true`, the variable will be passed only to pipelines running on protected branches and tags. Defaults to `false`
#   # masked (Boolean) If set to `true`, the variable will be masked if it would have been written to the logs. Defaults to `false`
#   # environment_scope (String) The environment_scope of the variable. Defaults to `*`
# }

# outputs
output "network_id_1" {
  value = module.test-vpc-module-1.network_id
}

output "subnets_ids_1" {
  value = module.test-vpc-module-1.subnets_ids
}

output "created_workspace_id_1" {
  value = module.tfe-module-1.created_workspace_id
}
