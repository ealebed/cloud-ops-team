# terraform configuration
terraform {
  required_version = ">= 0.13"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.53, < 5.0"
    }
    tfe = {
      version = "0.30.2"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.13.0"
    }
  }

  backend "remote" {
    hostname = "app.terraform.io"
    organization = "ealebed"

    workspaces {
      name = "2nd-layer-workspace"
    }
  }
}

# Common for all labs
locals {
  local_data = jsondecode(var.varSet)
}

# Configure Google Provider
provider "google" {
  project = local.local_data.gcp_project
  region  = local.local_data.gcp_region
  zone    = local.local_data.gcp_zone
}

# Configure Terraform Provider
provider "tfe" {
  hostname = local.local_data.tfe_hostname
  token    = local.local_data.tfe_token
}

# Configure GitLab Provider
provider "gitlab" {
  token = local.local_data.gitlab_token
}

# Variables
variable "varSet" {}

output "policy_set_id" {
  value = local.local_data.tfe_policy_set_id
}
