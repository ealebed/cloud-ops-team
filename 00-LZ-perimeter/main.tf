# terraform configuration
terraform {
  required_version = ">= 0.13"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.53, < 5.0"
    }
    tfe = {
      version = "0.30.2"
    }
  }

  backend "remote" {
    hostname = "app.terraform.io"
    organization = "ealebed"

    workspaces {
      name = "1st-level-workspace"
    }
  }
}

# Decode variables from 1st level workspace
# https://app.terraform.io/app/ealebed/workspaces/1st-level-workspace/variables
locals {
  local_data = jsondecode(var.varSet)
}

# Variables
variable "varSet" {}

# Configure Google Provider
provider "google" {
  project = local.local_data.gcp_project
  region  = local.local_data.gcp_region
  zone    = local.local_data.gcp_zone
}

# Configure Terraform Provider
provider "tfe" {
  hostname = local.local_data.tfe_hostname
  token    = local.local_data.tfe_token
}

# Example: create KMS crypto key
# resource "google_kms_key_ring" "keyring" {
#   name     = "keyring-example"
#   location = "global"
# }

# resource "google_kms_crypto_key" "example-key" {
#   name            = "crypto-key-example"
#   key_ring        = google_kms_key_ring.keyring.id
#   rotation_period = "100000s"

#   lifecycle {
#     prevent_destroy = true
#   }
# }

# Create 2nd level workspace for keeping lab(s) resources state
module "tfe-module" {
  source                  = "./modules/tfe"

  tfe_organization        = local.local_data.tfe_organization
  tfe_workspace           = "2nd-layer-workspace"

  credentials_file        = local.local_data.credentials_file

  tfe_variables_set       = jsonencode({
    "gcp_project"             = local.local_data.gcp_project,
    "gcp_region"              = local.local_data.gcp_region,
    "gcp_zone"                = local.local_data.gcp_zone,
    "gcp_kms_key"             = "google_kms_crypto_key_example",
    "gitlab_token"            = local.local_data.gitlab_token,
    "gitlab_organization"     = local.local_data.gitlab_organization,
    "gitlab_policy_repo_name" = local.local_data.gitlab_policy_repo_name,
    "tfe_token"               = local.local_data.tfe_token,
    "tfe_hostname"            = local.local_data.tfe_hostname,
    "tfe_organization"        = local.local_data.tfe_organization,
    "tfe_policy_set_id"       = tfe_policy_set.sentinel-policies.id,
    "credentials_file"        = local.local_data.credentials_file}
  )
}

# Create OAuth client to GitLab to create Policy Set configuration from GitLab repo
resource "tfe_oauth_client" "oauth-client" {
  api_url          = "https://gitlab.com/api/v4"
  http_url         = "https://gitlab.com"
  oauth_token      = local.local_data.gitlab_token
  organization     = local.local_data.tfe_organization
  service_provider = "gitlab_hosted"
}

# Create Policy Set and attach to this policy set just created 2nd level workspace
resource "tfe_policy_set" "sentinel-policies" {
  name          = "sentinel-policies"
  description   = "A Sentinel policy set for Terraform code"
  organization  = local.local_data.tfe_organization
  workspace_ids = [ module.tfe-module.created_workspace_id ]

  vcs_repo {
    identifier         = "${local.local_data.gitlab_organization}/${local.local_data.gitlab_policy_repo_name}"
    branch             = "master"
    ingress_submodules = false
    oauth_token_id     = tfe_oauth_client.oauth-client.oauth_token_id
  }
}

# Outputs
output "policy_set_id" {
  value = tfe_policy_set.sentinel-policies.id
}
