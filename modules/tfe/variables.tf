variable "tfe_organization" {
    description = "The Terraform Cloud/Enterprise organization to apply your changes to"
    type = string
}

variable "tfe_workspace" {
    description = "Workspace name in Terraform Cloud/Enterprise to create"
    type = string
}

variable "tfe_variables_set" {
    description = "Variables (composed to JSON and encoded to string) attached to workspaces as one simple 'terraform' variable"
    type = string
}

variable "credentials_file" {
    description = "The file with SA key to authenticate with Google Cloud. File will be added to Terraform Cloud/Enterprise as variable"
    type = string
}
