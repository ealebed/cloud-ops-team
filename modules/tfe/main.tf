data "local_file" "credentials" {
  filename = var.credentials_file
}

resource "tfe_workspace" "workspace" {
  name         = var.tfe_workspace
  organization = var.tfe_organization
}

resource "tfe_variable" "variables-set" {  
  key          = "varSet"
  value        = var.tfe_variables_set
  category     = "terraform"
  sensitive    = false
  workspace_id = tfe_workspace.workspace.id
  description  = "All variables composed to JSON and encoded to string value"
}

resource "tfe_variable" "environment-var" {
  key          = "GOOGLE_CREDENTIALS"
  value        = data.local_file.credentials.content
  category     = "env"
  sensitive    = true
  workspace_id = tfe_workspace.workspace.id
  description  = "Google Cloud service account credentials"
}
