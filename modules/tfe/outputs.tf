output "created_workspace_id" {
  value = tfe_workspace.workspace.id
}

# output "workspace_ids" {
#   value = [ for ws in tfe_workspace.workspaces: ws.id ]
# }

# output "workspace_names" {
#   value = [ for ws in tfe_workspace.workspaces: ws.name ]
# }

# output "oauth_token_id" {
#   value = tfe_oauth_client.oauth-client.oauth_token_id
# }

# output "existing_workspaces_list" {
#   value = keys(data.tfe_workspace_ids.existing.full_names)
# }

# output "existing_workspaces_ids" {
#   value = values(data.tfe_workspace_ids.existing.ids)
# }
